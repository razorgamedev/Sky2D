import 
    Sky2D/sky_window,
    Sky2D/sky_renderer,
    Sky2D/sky_color,
    Sky2D/sky_image,
    Sky2D/sky_input,
    glfw

var window = newSkyWindow()
var renderer = newSkyRenderer(window)
var timer = 0.0

# var img = loadImage "Tests/test_img.png"

while window.isOpen:
    timer += 0.0016'f32

    window.update(exit_on_escape = true)
    
    renderer.clear()
    
    renderer.begin2D()
    renderer.drawRect(128, 128, 128, 128, timer, newCyan())
    renderer.end2D()
    