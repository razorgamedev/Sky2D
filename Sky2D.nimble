# Package

version       = "0.0.1"
author        = "SkyVaultGames"
description   = "A simple yet powerful 2d game framework"
license       = "MIT"

# Dependencies

requires "nim >= 0.17.0"
requires "nim-glfw >= 0.2.1"
requires "opengl >= 0.0.0"
requires "https://github.com/define-private-public/stb_image-Nim"
requires "glm >= 0.1.1"