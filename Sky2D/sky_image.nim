import
    opengl,
    glu,
    sky_math,
    stb_image/read as stbi

type
    SkyImage* = ref object of RootObj
        id: GLuint
        width*, height* :int

proc start* (img: SkyImage)=
    glBindTexture(GL_TEXTURE_2D, img.id)

proc stop* (img: SkyImage)=
    glBindTexture(GL_TEXTURE_2D, 0)

proc loadImage* (path: string): SkyImage=
    var img = SkyImage(id: 0, width: 0, height: 0)

    var
        width, height, channels: int
        data: seq[uint8]

    
    data = stbi.load(path, width, height, channels, stbi.RGBA)
    
    img.width = width
    img.height = height

    # handle data using opengl
    glGenTextures(1, addr(img.id))
    glBindTexture(GL_TEXTURE_2D, img.id)

    if img.id <= 0:
        echo "ERROR:: failed to load image!"

    #NOTE: I dont know why i need to subtract 1 :(
    glTexImage2D(
        GL_TEXTURE_2D, 
        (GLint)0, 
        (GLint)GL_RGBA, 
        (GLsizei)img.width, 
        (GLsizei)img.height, 
        (GLint)0, 
        GL_RGBA, 
        GL_UNSIGNED_BYTE, 
        cast[pointer](data[0].addr)
        )

    #NOTE: @Important the user might not want it to be nearest neighbor 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)

    return img