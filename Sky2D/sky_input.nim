import
    glfw,
    glfw/wrapper,
    tables,
    system

type
    SkyKey = ref object of RootObj
        state* , last* : int

    SkyInputManager* = ref object of RootObj
        mouse_x, mouse_y: float64
        mouse_dx, mouse_dy: float
        last_mouse_x, last_mouse_y: float

        the_first: bool
        the_block: bool

        last_mouse_left_state, curr_mouse_left_state: bool
        last_mouse_right_state, curr_mouse_right_state: bool
        
proc newInputManager* (): SkyInputManager=
    return SkyInputManager(
        mouse_x: 0, mouse_y: 0,
        mouse_dx: 0, mouse_dy: 0,
        last_mouse_x: 0, last_mouse_y: 0,
        the_first: false, the_block: false,
        last_mouse_left_state: false, curr_mouse_left_state: false,
        last_mouse_right_state: false, curr_mouse_right_state: false
    )

proc newKey(): SkyKey=
    return SkyKey(state: 0, last: 0)

proc newKey(s: cint, ll: cint): SkyKey=
    return SkyKey(state: s, last: ll)

var SkyInput* = newInputManager()
var keyMap: Table[cint, SkyKey] = initTable[cint, SkyKey]()

proc getMouseX* (input: SkyInputManager): float= 
    return (float)input.mouse_x

proc getMouseY* (input: SkyInputManager): float= 
    return (float)input.mouse_y

proc getMouseDeltaX* (input: SkyInputManager): float= 
    return (float)input.mouse_dx

proc getMouseDeltaY* (input: SkyInputManager): float= 
    return (float)input.mouse_dy

proc isMouseLeftDown*(input: SkyInputManager): bool=
    var win = getCurrentContext()
    return win.getMouseButton(0) == 1

proc isMouseRightDown*(input: SkyInputManager): bool=
    var win = getCurrentContext()
    return win.getMouseButton(1) == 1

proc isMouseLeftPressed*(input: SkyInputManager): bool =
    var win = getCurrentContext()
    input.curr_mouse_left_state = win.getMouseButton(0) == 1
    if (input.curr_mouse_left_state and not input.last_mouse_left_state):
        return true
    return false

proc isMouseRightPressed*(input: SkyInputManager): bool =
    var win = getCurrentContext()
    input.curr_mouse_right_state = win.getMouseButton(1) == 1
    if (input.curr_mouse_right_state and not input.last_mouse_right_state):
        return true
    return false

proc isKeyPressed* (input: SkyInputManager, key: Key): bool=
    var win = getCurrentContext()
    if (input.the_block): return false
    var ckey = cast[cint](key)
    if (not keyMap.contains ckey):
        var mykey = newKey()
        keyMap.add ckey, mykey
    else:
        var k = keyMap[ckey]
        k.state = win.getKey(ckey)
        keyMap[ckey] = k
        if (k.state == 1 and k.last == 0):
            return true 
        return false

proc isKeyDown* (input: SkyInputManager, key: Key): bool =
    var win = getCurrentContext()
    var ckey = cast[cint](key)
    if not keyMap.contains ckey:
        var k = newKey()
        k.state = win.getKey(ckey)
        k.last = k.state
        keyMap.add ckey, k
    else:
        keyMap[ckey].state = win.getKey(ckey)
        return keyMap[ckey].state == 1


proc update* (input: SkyInputManager)=
    var win = getCurrentContext()
    win.getCursorPos(addr input.mouse_x, addr input.mouse_y)

    for key in keyMap.pairs:
        var k = keyMap[key[0]]
        k.last = k.state
        keyMap[key[0]] = k

    if (input.last_mouse_x < 0 or input.last_mouse_y < 0):
        input.last_mouse_x = input.getMouseX()
        input.last_mouse_y = input.getMouseY()
    else:
        var mx = input.getMouseX()
        var my = input.getMouseY()
        input.mouse_dx = mx - input.last_mouse_x
        input.mouse_dy = my - input.last_mouse_y
    
    input.last_mouse_left_state = input.curr_mouse_left_state
    input.last_mouse_right_state = input.curr_mouse_right_state

    input.the_first = false