import 
    math,
    os,
    glfw,
    glfw/wrapper,
    opengl,
    glu,
    sky_color,
    sky_input

type
    SkyGameTimer* = ref object of RootObj
        fps* : float 
        deltaTime* : float
        totalTicks* : int64
        timer* : float64
        lastTime* :float

    SkyWindow* = ref object of RootObj
        win: Win
        timer: SkyGameTimer

    SkyConfig* = ref object of RootObj
        width, height: int
        title: string

proc newConfig* (width: int, height: int, title: string = ""): SkyConfig =
    return SkyConfig(
        width: width,
        height: height,
        title: title
    )

proc initializeOpenGL(win: SkyWindow) =
    win.win.makeContextCurrent()
    loadExtensions()
    glEnable(GL_CULL_FACE)

proc isOpen* (win: SkyWindow): bool =
    return win.win.shouldClose() == false

proc getSize * (win: SkyWindow): tuple[x: int, y: int] =
    var size = win.win.size()
    return (size[0], size[1])

# Timer stuff
proc getFPS* (win: SkyWindow): float=
    return win.timer.fps

proc getDeltaTime* (win: SkyWindow): float=
    return win.timer.deltaTime

proc getTotalTicks* (win: SkyWindow): int64=
    return win.timer.totalTicks

proc getTimer* (win: SkyWindow): float64=
    return win.timer.timer

proc update * (win: SkyWindow, exit_on_escape = false)=
    var size = win.getSize()
    glViewport((GLint)0, (GLint)0, (GLsizei)size[0], (GLsizei)size[1])
    win.win.swapBufs()
    pollEvents()
    
    SkyInput.update()

    var now = getTime()
    win.timer.deltaTime = (now - win.timer.lastTime) / 1000
    win.timer.lastTime = now
    win.timer.fps = 1 / win.timer.deltaTime
    win.timer.totalTicks += 1
    win.timer.timer += win.timer.deltaTime

    if (exit_on_escape):
        if win.win.isKeyDown(keyEscape):
            win.win.shouldClose = true

proc newSkyWindow * (): SkyWindow =
    glfw.init()
    let win = SkyWindow(
        win: newGlWin(
            dim = (w: 800, h: 480),
            title = "Hello World",
            resizable = true 
        ),
        timer: SkyGameTimer(
            fps: 0,
            deltaTime: 0,
            totalTicks: 0,
            timer: 0,
            lastTime: getTime()
        )
    )
    win.initializeOpenGL()

    return win