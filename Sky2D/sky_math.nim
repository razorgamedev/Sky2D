import 
    strutils

type
    Vec2* = ref object of RootObj
        x* , y* : float
    
    Vec3* = ref object of RootObj
        x* , y* , z* : float


proc newVec2* (x: float, y: float): Vec2=
    return Vec2(x: x, y: y)

proc lerp* (a: float, b:float, t:float): float=
    return a + (b - a) * t