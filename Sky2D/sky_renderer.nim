import
    sky_color,
    sky_window,
    sky_image,
    glfw,
    opengl,
    glu,
    glm,
    math

type
    SkyRenderer* = ref object of RootObj
        clear_color: Color
        win: Sky_Window

    SkyRegion* = ref object of RootObj
        x*,y*, width*, height* : uint
        
proc newRegion* (x: uint, y: uint, w: uint, h: uint): SkyRegion=
    return SkyRegion(
        x: x, y: y, width: w, height: h
    )

proc newSkyRenderer* (win: SkyWindow): SkyRenderer =
    return SkyRenderer(
        clear_color: newDarkGray(),
        win: win
    )

proc setClearColor* (ren: SkyRenderer, col: Color)=
    ren.clear_color = col
        
proc clear* (ren: SkyRenderer)=
    glClear(GL_COLOR_BUFFER_BIT)
    glClearColor(ren.clear_color.r, ren.clear_color.g, ren.clear_color.b, ren.clear_color.a)

proc begin2D* (ren: SkyRenderer)=
    # Enable opengl stuff
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()

    # let size = ren.win.getSize()
    var matrix = ortho(0.0, 800'f32, 480'f32, 0.0, -1.0, 1.0)
    glLoadMatrixd(matrix.caddr)    # glPushMatrix()
    
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

proc end2D* (ren: SkyRenderer)=
    # Cleanup and disable opengl stuff
    glDisable(GL_TEXTURE_2D)

proc drawRect* (ren: SkyRenderer, x: float, y: float, w: float, h: float, rot: float = 0, col: Color = newWhite())=
    var whi = newWhite()
    
    glPushMatrix();
    glTranslatef(x, y, 0);

    glTranslatef(w / 2, h / 2, 0);
    glRotatef(rot, 0, 0, 1);
    glTranslatef(-w / 2,-h / 2 , 0);

    glBegin(GL_QUADS);
    glColor4f(col.r, col.g, col.b, col.a);

    glVertex2f(0, 0);
    glVertex2f(0, h);
    glVertex2f(w, h);
    glVertex2f(w, 0);

    glColor4f(whi.r, whi.g, whi.b, whi.a);
    glEnd();

    glPopMatrix();

proc drawLine* (ren: SkyRenderer, ax: float, ay: float, bx: float, by: float, col: Color = newWhite())=
    var whi = newWhite()

    glBegin(GL_LINES);
    glColor4f(col.r, col.g, col.b, col.a);

    glVertex2f(ax, ay);
    glVertex2f(bx, by);

    glColor4f(whi.r, whi.g, whi.b, whi.a);
    glEnd();

proc drawImage* (ren: SkyRenderer, img: SkyImage, x: float, y:float, w: float, h:float, rot:float = 0, col: Color = newWhite())=
    glEnable(GL_TEXTURE_2D)
    var whi = newWhite()
    img.start()
    defer: img.stop()
    
    glPushMatrix();
    glTranslatef(x, y, 0);

    glTranslatef(w / 2, h / 2, 0);
    glRotatef(rot, 0, 0, 1);
    glTranslatef(-w / 2,-h / 2 , 0);

    glBegin(GL_QUADS);
    glColor4f(col.r, col.g, col.b, col.a);

    glTexCoord2f(0, 0); glVertex2f(0, 0);
    glTexCoord2f(0, 1); glVertex2f(0, h);
    glTexCoord2f(1, 1); glVertex2f(w, h);
    glTexCoord2f(1, 0); glVertex2f(w, 0);

    glColor4f(whi.r, whi.g, whi.b, whi.a);
    glEnd();

    glPopMatrix();
    glDisable(GL_TEXTURE_2D)

proc drawImageRegion*(ren: SkyRenderer, img: SkyImage, reg: SkyRegion, x: float, y:float, w: float, h: float, rot: float = 0)=
    glEnable(GL_TEXTURE_2D)
    var tw = (float)img.width
    var th = (float)img.height

    var uvx:float = (float)(reg.x) / tw;
    var uvy:float = 1 - (float)(reg.y) / th;

    var uvw:float = (float)(reg.width) /  tw;
    var uvh:float = (float)(reg.height) / th;
    # uvy -= uvh;

    img.start()
    defer: img.stop()

    glPushMatrix();
    glTranslatef(x, y, 0);
    
    glTranslatef(w / 2, h / 2, 0);
    glRotatef(rot, 0, 0, 1);
    glTranslatef(-w / 2, -h / 2, 0);

    glBegin(GL_QUADS);

    glTexCoord2f(uvx, uvy);             glVertex2f(0, 0);
    glTexCoord2f(uvx, uvy + uvw);       glVertex2f(0, h);
    glTexCoord2f(uvx + uvw, uvy + uvh); glVertex2f(w, h);
    glTexCoord2f(uvx + uvw,uvy);        glVertex2f(w, 0);

    glEnd();

    glPopMatrix();
    glDisable(GL_TEXTURE_2D)
