import
    sky_math

type 
    Color* = ref object of RootObj
        r *, g *, b *, a *: float

proc newColor* (r:float = 0, g:float= 0, b:float= 0, a:float= 1): Color =
    return Color(r: r,g: g,b: b,a: a)

proc lerpColor* (a: Color, b: Color, t:float): Color =
    var color: Color = newColor()
    color.r = lerp(a.r, b.r, t)
    color.g = lerp(a.g, b.g, t)
    color.b = lerp(a.b, b.b, t)
    color.a = lerp(a.a, b.a, t)
    return color

proc newRed*        (): Color = return newColor(1, 0, 0, 1)
proc newGreen*      (): Color = return newColor(0, 1, 0, 1)
proc newBlue*       (): Color = return newColor(0, 0, 1, 1)
proc newBlack*      (): Color = return newColor(0, 0, 0, 1)
proc newDarkGray*   (): Color = return newColor(0.2, 0.2, 0.2, 1)
proc newGray*       (): Color = return newColor(0.5, 0.5, 0.5, 1)
proc newLightGray*  (): Color = return newColor(0.8, 0.8, 0.8, 1)
proc newWhite*      (): Color = return newColor(1, 1, 1, 1)
proc newTransperent*(): Color = return newColor(0, 0, 0, 0)
proc newYellow*     (): Color = return newColor(1, 1, 0, 1)
proc newMagenta*    (): Color = return newColor(1, 0, 1, 1)
proc newCyan*       (): Color = return newColor(0, 1, 1, 1)
proc newOrange*     (): Color = return newColor(1.0, 0.5, 0.0, 1.0)


