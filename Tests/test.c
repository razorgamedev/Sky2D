int tw, th; tex.Get_Size(&tw, &th);
	
float uvx = (float)r.x / (float)tw;
float uvy = 1 - ((float)r.y / (float)th);

float uvw = (float)(r.width / (float)tw);
float uvh = (float)(r.height / (float)th);
uvy -= uvh;

tex.Bind();

glPushMatrix();
glTranslatef(x, y, 0);

glTranslatef(w / 2, h / 2, 0);
glRotatef(rot, 0, 0, 1);
glTranslatef(-w / 2, -h / 2, 0);

glBegin(GL_QUADS);

glTexCoord2f(uvx,			  uvy);		glVertex2f(0, h);
glTexCoord2f(uvx,		uvy + uvw);		glVertex2f(0, 0);
glTexCoord2f(uvx + uvw, uvy + uvh);		glVertex2f(w, 0);
glTexCoord2f(uvx + uvw,		  uvy);		glVertex2f(w, h);

glEnd();

glPopMatrix();
tex.Unbind();