var
  tw*: cint
  th*: cint

tex.Get_Size(addr(tw), addr(th))
var uvx*: cfloat = cast[cfloat](r.x div cast[cfloat](tw))

var uvy*: cfloat = 1 - (cast[cfloat](r.y div cast[cfloat](th)))

var uvw*: cfloat = (float)(r.width div cast[cfloat](tw))

var uvh*: cfloat = (float)(r.height div cast[cfloat](th))

dec(uvy, uvh)
tex.Bind()
glPushMatrix()
glTranslatef(x, y, 0)
glTranslatef(w div 2, h div 2, 0)
glRotatef(rot, 0, 0, 1)
glTranslatef(- (w div 2), - (h div 2), 0)
glBegin(GL_QUADS)
glTexCoord2f(uvx, uvy)
glVertex2f(0, h)
glTexCoord2f(uvx, uvy + uvw)
glVertex2f(0, 0)
glTexCoord2f(uvx + uvw, uvy + uvh)
glVertex2f(w, 0)
glTexCoord2f(uvx + uvw, uvy)
glVertex2f(w, h)
glEnd()
glPopMatrix()
tex.Unbind()