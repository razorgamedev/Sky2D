/* Generated by Nim Compiler v0.17.0 */
/*   (c) 2017 Andreas Rumpf */
/* The generated code is subject to the original license. */
/* Compiled for: Windows, amd64, gcc */
/* Command for C compiler:
   C:\Users\dusti\Apps\nim-0.17.0\dist\mingw64\bin\gcc.exe -c  -w -D_GLFW_USE_OPENGL -D_GLFW_WIN32 -D_GLFW_WGL  -IC:\Users\dusti\Apps\nim-0.17.0\lib -o C:\Users\dusti\Repos\Sky2D\nimcache\Sky2D_sky_image.o C:\Users\dusti\Repos\Sky2D\nimcache\Sky2D_sky_image.c */
#define NIM_NEW_MANGLING_RULES
#define NIM_INTBITS 64

#include "nimbase.h"
#include <stdio.h>
#undef linux
#undef near
typedef struct SkyImagecolonObjectType__hUip36kEQaC9c3pGMVUQ56g SkyImagecolonObjectType__hUip36kEQaC9c3pGMVUQ56g;
typedef struct TNimObject TNimObject;
typedef struct TNimType TNimType;
typedef struct TNimNode TNimNode;
typedef struct NimStringDesc NimStringDesc;
typedef struct TGenericSeq TGenericSeq;
typedef struct TY_6H5Oh5UUvVCLiakt9aTwtUQ TY_6H5Oh5UUvVCLiakt9aTwtUQ;
typedef NU8 TNimKind_jIBKr1ejBgsfM33Kxw4j7A;
typedef NU8 TNimTypeFlag_v8QUszD1sWlSIWZz7mC4bQ_Set;
typedef N_NIMCALL_PTR(void, TY_ojoeKfW4VYIm36I9cpDTQIg) (void* p, NI op);
typedef N_NIMCALL_PTR(void*, TY_WSm2xU5ARYv9aAR4l0z9c9auQ) (void* p);
struct  TNimType  {
NI size;
TNimKind_jIBKr1ejBgsfM33Kxw4j7A kind;
TNimTypeFlag_v8QUszD1sWlSIWZz7mC4bQ_Set flags;
TNimType* base;
TNimNode* node;
void* finalizer;
TY_ojoeKfW4VYIm36I9cpDTQIg marker;
TY_WSm2xU5ARYv9aAR4l0z9c9auQ deepcopy;
};
struct  TNimObject  {
TNimType* m_type;
};
struct  SkyImagecolonObjectType__hUip36kEQaC9c3pGMVUQ56g  {
  TNimObject Sup;
NU32 id;
NI width;
NI height;
};
struct  TGenericSeq  {
NI len;
NI reserved;
};
struct  NimStringDesc  {
  TGenericSeq Sup;
NIM_CHAR data[SEQ_DECL_SIZE];
};
typedef NU8 TNimNodeKind_unfNsxrcATrufDZmpBq4HQ;
struct  TNimNode  {
TNimNodeKind_unfNsxrcATrufDZmpBq4HQ kind;
NI offset;
TNimType* typ;
NCSTRING name;
NI len;
TNimNode** sons;
};
struct TY_6H5Oh5UUvVCLiakt9aTwtUQ {
  TGenericSeq Sup;
  NU8 data[SEQ_DECL_SIZE];
};
N_NIMCALL(void, start_81Tw9b9aXaFfXLhQi0iMFMQg)(SkyImagecolonObjectType__hUip36kEQaC9c3pGMVUQ56g* img);
N_STDCALL(void, glBindTexture_shJfrHWoymy1BbmsKpC7Xg)(NU32 target, NU32 texture);
static N_INLINE(void, nimFrame)(TFrame* s);
N_NOINLINE(void, stackOverflow_II46IjNZztN9bmbxUD8dt8g)(void);
static N_INLINE(void, popFrame)(void);
N_NIMCALL(void, stop_81Tw9b9aXaFfXLhQi0iMFMQg_2)(SkyImagecolonObjectType__hUip36kEQaC9c3pGMVUQ56g* img);
N_NIMCALL(SkyImagecolonObjectType__hUip36kEQaC9c3pGMVUQ56g*, loadImage_7rAFm9ad3lntqW9bI65tCm5g)(NimStringDesc* path);
static N_NIMCALL(void, Marker_TY_VaHnamzU9beBAr7eHOECELg)(void* p, NI op);
N_NIMCALL(void*, newObj)(TNimType* typ, NI size);
N_NIMCALL(TY_6H5Oh5UUvVCLiakt9aTwtUQ*, load_IpqKxisbLcaIMgU5vJeYeQ)(NimStringDesc* filename, NI* x, NI* y, NI* channels_in_file, NI desired_channels);
N_STDCALL(void, glGenTextures_WoDI9cP6rS3Osykz9bfiBK6w)(NI32 n, NU32* textures);
N_STDCALL(void, glTexImage2D_G7UzQ8ZECowVV7IoQf5rhg)(NU32 target, NI32 level, NI32 internalformat, NI32 width, NI32 height, NI32 border, NU32 format, NU32 type_0, void* pixels);
static N_INLINE(NI, chckRange)(NI i, NI a, NI b);
N_NOINLINE(void, raiseRangeError)(NI64 val);
N_NOINLINE(void, raiseIndexError)(void);
N_STDCALL(void, glTexParameteri_MG1n9caM41cv9bizyIOehN3g)(NU32 target, NU32 pname, NI32 param);
extern TFrame* framePtr_HRfVMH3jYeBJz6Q6X9b6Ptw;
extern TNimType NTI_13RNkKqUOX1TtorOUlKtqA_;
TNimType NTI_hUip36kEQaC9c3pGMVUQ56g_;
extern TNimType NTI_JrFyJbYm9b5I4hJ9cWDqvfYA_;
extern TNimType NTI_rR5Bzr1D5krxoo1NcNyeMA_;
TNimType NTI_VaHnamzU9beBAr7eHOECELg_;
STRING_LITERAL(TM_A9cbq2t9bNhiPkF1ZXyOzRUw_3, "ERROR:: failed to load image!", 29);

static N_INLINE(void, nimFrame)(TFrame* s) {
	NI T1_;
	T1_ = (NI)0;
	{
		if (!(framePtr_HRfVMH3jYeBJz6Q6X9b6Ptw == NIM_NIL)) goto LA4_;
		T1_ = ((NI) 0);
	}
	goto LA2_;
	LA4_: ;
	{
		T1_ = ((NI) ((NI16)((*framePtr_HRfVMH3jYeBJz6Q6X9b6Ptw).calldepth + ((NI16) 1))));
	}
	LA2_: ;
	(*s).calldepth = ((NI16) (T1_));
	(*s).prev = framePtr_HRfVMH3jYeBJz6Q6X9b6Ptw;
	framePtr_HRfVMH3jYeBJz6Q6X9b6Ptw = s;
	{
		if (!((*s).calldepth == ((NI16) 2000))) goto LA9_;
		stackOverflow_II46IjNZztN9bmbxUD8dt8g();
	}
	LA9_: ;
}

static N_INLINE(void, popFrame)(void) {
	framePtr_HRfVMH3jYeBJz6Q6X9b6Ptw = (*framePtr_HRfVMH3jYeBJz6Q6X9b6Ptw).prev;
}

N_NIMCALL(void, start_81Tw9b9aXaFfXLhQi0iMFMQg)(SkyImagecolonObjectType__hUip36kEQaC9c3pGMVUQ56g* img) {
	nimfr_("start", "sky_image.nim")
	nimln_(13, "sky_image.nim");
	glBindTexture_shJfrHWoymy1BbmsKpC7Xg(((NU32) 3553), (*img).id);
	popFrame();
}

N_NIMCALL(void, stop_81Tw9b9aXaFfXLhQi0iMFMQg_2)(SkyImagecolonObjectType__hUip36kEQaC9c3pGMVUQ56g* img) {
	nimfr_("stop", "sky_image.nim")
	nimln_(16, "sky_image.nim");
	glBindTexture_shJfrHWoymy1BbmsKpC7Xg(((NU32) 3553), ((NU32) 0));
	popFrame();
}
static N_NIMCALL(void, Marker_TY_VaHnamzU9beBAr7eHOECELg)(void* p, NI op) {
	SkyImagecolonObjectType__hUip36kEQaC9c3pGMVUQ56g* a;
	a = (SkyImagecolonObjectType__hUip36kEQaC9c3pGMVUQ56g*)p;
}

static N_INLINE(NI, chckRange)(NI i, NI a, NI b) {
	NI result;
{	result = (NI)0;
	{
		NIM_BOOL T3_;
		T3_ = (NIM_BOOL)0;
		T3_ = (a <= i);
		if (!(T3_)) goto LA4_;
		T3_ = (i <= b);
		LA4_: ;
		if (!T3_) goto LA5_;
		result = i;
		goto BeforeRet_;
	}
	goto LA1_;
	LA5_: ;
	{
		raiseRangeError(((NI64) (i)));
	}
	LA1_: ;
	}BeforeRet_: ;
	return result;
}

N_NIMCALL(SkyImagecolonObjectType__hUip36kEQaC9c3pGMVUQ56g*, loadImage_7rAFm9ad3lntqW9bI65tCm5g)(NimStringDesc* path) {
	SkyImagecolonObjectType__hUip36kEQaC9c3pGMVUQ56g* result;
	SkyImagecolonObjectType__hUip36kEQaC9c3pGMVUQ56g* img;
	SkyImagecolonObjectType__hUip36kEQaC9c3pGMVUQ56g* T1_;
	NI width;
	NI height;
	NI channels;
	TY_6H5Oh5UUvVCLiakt9aTwtUQ* data;
	nimfr_("loadImage", "sky_image.nim")
{	result = (SkyImagecolonObjectType__hUip36kEQaC9c3pGMVUQ56g*)0;
	nimln_(19, "sky_image.nim");
	T1_ = (SkyImagecolonObjectType__hUip36kEQaC9c3pGMVUQ56g*)0;
	T1_ = (SkyImagecolonObjectType__hUip36kEQaC9c3pGMVUQ56g*) newObj((&NTI_VaHnamzU9beBAr7eHOECELg_), sizeof(SkyImagecolonObjectType__hUip36kEQaC9c3pGMVUQ56g));
	(*T1_).Sup.m_type = (&NTI_hUip36kEQaC9c3pGMVUQ56g_);
	(*T1_).id = ((NU32) 0);
	(*T1_).width = ((NI) 0);
	(*T1_).height = ((NI) 0);
	img = T1_;
	width = (NI)0;
	height = (NI)0;
	channels = (NI)0;
	data = (TY_6H5Oh5UUvVCLiakt9aTwtUQ*)0;
	nimln_(26, "sky_image.nim");
	data = load_IpqKxisbLcaIMgU5vJeYeQ(path, (&width), (&height), (&channels), ((NI) 4));
	nimln_(28, "sky_image.nim");
	(*img).width = width;
	nimln_(29, "sky_image.nim");
	(*img).height = height;
	nimln_(32, "sky_image.nim");
	glGenTextures_WoDI9cP6rS3Osykz9bfiBK6w(((NI32) 1), (&(*img).id));
	nimln_(33, "sky_image.nim");
	glBindTexture_shJfrHWoymy1BbmsKpC7Xg(((NU32) 3553), (*img).id);
	nimln_(35, "sky_image.nim");
	{
		if (!((NU64)((*img).id) <= (NU64)(((NI) 0)))) goto LA4_;
		nimln_(36, "sky_image.nim");
		printf("%s\015\012", ((NimStringDesc*) &TM_A9cbq2t9bNhiPkF1ZXyOzRUw_3)? (((NimStringDesc*) &TM_A9cbq2t9bNhiPkF1ZXyOzRUw_3))->data:"nil");
		fflush(stdout);
	}
	LA4_: ;
	nimln_(40, "sky_image.nim");
	if ((NU)(((NI) 0)) >= (NU)(data->Sup.len)) raiseIndexError();
	glTexImage2D_G7UzQ8ZECowVV7IoQf5rhg(((NU32) 3553), ((NI32) 0), ((NI32) 6408), ((NI32)chckRange((*img).width, ((NI32) (-2147483647 -1)), ((NI32) 2147483647))), ((NI32)chckRange((*img).height, ((NI32) (-2147483647 -1)), ((NI32) 2147483647))), ((NI32) 0), ((NU32) 6408), ((NU32) 5121), ((void*) ((&data->data[((NI) 0)]))));
	nimln_(52, "sky_image.nim");
	glTexParameteri_MG1n9caM41cv9bizyIOehN3g(((NU32) 3553), ((NU32) 10240), ((NI32) 9728));
	nimln_(53, "sky_image.nim");
	glTexParameteri_MG1n9caM41cv9bizyIOehN3g(((NU32) 3553), ((NU32) 10241), ((NI32) 9728));
	nimln_(55, "sky_image.nim");
	result = img;
	goto BeforeRet_;
	}BeforeRet_: ;
	popFrame();
	return result;
}
NIM_EXTERNC N_NOINLINE(void, Sky2D_sky_imageInit000)(void) {
	nimfr_("sky_image", "sky_image.nim")
	popFrame();
}

NIM_EXTERNC N_NOINLINE(void, Sky2D_sky_imageDatInit000)(void) {
static TNimNode* TM_A9cbq2t9bNhiPkF1ZXyOzRUw_2[3];
static TNimNode TM_A9cbq2t9bNhiPkF1ZXyOzRUw_0[4];
NTI_hUip36kEQaC9c3pGMVUQ56g_.size = sizeof(SkyImagecolonObjectType__hUip36kEQaC9c3pGMVUQ56g);
NTI_hUip36kEQaC9c3pGMVUQ56g_.kind = 17;
NTI_hUip36kEQaC9c3pGMVUQ56g_.base = (&NTI_13RNkKqUOX1TtorOUlKtqA_);
NTI_hUip36kEQaC9c3pGMVUQ56g_.flags = 1;
TM_A9cbq2t9bNhiPkF1ZXyOzRUw_2[0] = &TM_A9cbq2t9bNhiPkF1ZXyOzRUw_0[1];
TM_A9cbq2t9bNhiPkF1ZXyOzRUw_0[1].kind = 1;
TM_A9cbq2t9bNhiPkF1ZXyOzRUw_0[1].offset = offsetof(SkyImagecolonObjectType__hUip36kEQaC9c3pGMVUQ56g, id);
TM_A9cbq2t9bNhiPkF1ZXyOzRUw_0[1].typ = (&NTI_JrFyJbYm9b5I4hJ9cWDqvfYA_);
TM_A9cbq2t9bNhiPkF1ZXyOzRUw_0[1].name = "id";
TM_A9cbq2t9bNhiPkF1ZXyOzRUw_2[1] = &TM_A9cbq2t9bNhiPkF1ZXyOzRUw_0[2];
TM_A9cbq2t9bNhiPkF1ZXyOzRUw_0[2].kind = 1;
TM_A9cbq2t9bNhiPkF1ZXyOzRUw_0[2].offset = offsetof(SkyImagecolonObjectType__hUip36kEQaC9c3pGMVUQ56g, width);
TM_A9cbq2t9bNhiPkF1ZXyOzRUw_0[2].typ = (&NTI_rR5Bzr1D5krxoo1NcNyeMA_);
TM_A9cbq2t9bNhiPkF1ZXyOzRUw_0[2].name = "width";
TM_A9cbq2t9bNhiPkF1ZXyOzRUw_2[2] = &TM_A9cbq2t9bNhiPkF1ZXyOzRUw_0[3];
TM_A9cbq2t9bNhiPkF1ZXyOzRUw_0[3].kind = 1;
TM_A9cbq2t9bNhiPkF1ZXyOzRUw_0[3].offset = offsetof(SkyImagecolonObjectType__hUip36kEQaC9c3pGMVUQ56g, height);
TM_A9cbq2t9bNhiPkF1ZXyOzRUw_0[3].typ = (&NTI_rR5Bzr1D5krxoo1NcNyeMA_);
TM_A9cbq2t9bNhiPkF1ZXyOzRUw_0[3].name = "height";
TM_A9cbq2t9bNhiPkF1ZXyOzRUw_0[0].len = 3; TM_A9cbq2t9bNhiPkF1ZXyOzRUw_0[0].kind = 2; TM_A9cbq2t9bNhiPkF1ZXyOzRUw_0[0].sons = &TM_A9cbq2t9bNhiPkF1ZXyOzRUw_2[0];
NTI_hUip36kEQaC9c3pGMVUQ56g_.node = &TM_A9cbq2t9bNhiPkF1ZXyOzRUw_0[0];
NTI_VaHnamzU9beBAr7eHOECELg_.size = sizeof(SkyImagecolonObjectType__hUip36kEQaC9c3pGMVUQ56g*);
NTI_VaHnamzU9beBAr7eHOECELg_.kind = 22;
NTI_VaHnamzU9beBAr7eHOECELg_.base = (&NTI_hUip36kEQaC9c3pGMVUQ56g_);
NTI_VaHnamzU9beBAr7eHOECELg_.marker = Marker_TY_VaHnamzU9beBAr7eHOECELg;
}

